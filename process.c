#include <stdio.h> /* printf */
#include <stdlib.h> /* exit */
#include <unistd.h> /* fork */
#include <sys/wait.h> /* waitpid */
#include <sys/types.h> /* pid_t */
#include <stddef.h>
void process(int number, int time) {
	printf("Prosess %d kjører\n", number);
	sleep(time);
	printf("Prosess %d kjørte i %d sekunder\n", number, time);
}

/*
Slik ser prosessene som programmet skal kjøre ut. Og rellefølgen på de

5 |           S--------T
4 |   S--------T
3 |        S-----T
2 S--------T
1 |   S----T
0 S--T
+-----------------------> tid i sekunder
  0  1  2  3  4  5  6  7
*/

int main(void){
	pid_t pid;								//Prosess id

	pid = fork();							//Felles fork
	if (pid == 0){							//Child p 0,1,3,5
		pid = fork();
		if (pid == 0) {
			process(0, 1);					//Starter p0
			exit(0);						//Dreper child
		}
		else{
			waitpid(pid, NULL, 0),			//Venter på p0
			pid = fork();
			if (pid == 0) {	
				process(1, 2);				//Starter p1
				exit(0);					//Dreper child
			}
			else{
				pid = fork();
				if (pid == 0){
					process(4, 3);			//Start p4
					exit(0);				//Dreper child
				}
				else {
					waitpid(pid, NULL, 0);	//Venter på p4
					process(5, 3);			//Starter p5
				}
			}
		}
	}
	else{
		pid = fork();
		if (pid == 0){
			process(2, 3);					//Starter p2
			exit(0);						//Dreper child
		}
		else{
			waitpid(pid, NULL, 0);			//Venter på p2
			process(3, 2);					//Starter p3
			exit(0);						//Dreper child
		}
	}
}
