# opsys_oblig1
Microsoft Windows v1709 m. "Bash on Ubuntu on Windows"
Description: Ubuntu 16.04.3 LTS
Realease: 16.04

Valideringsprogramm som kreves for kvalitetssikkring:
gcc, 
cppcheck, 
valgrind, 
cland-tidy-5.0.

Oblig1 Readme

Lage nye prosesser og enkel synkronisering av disse (4.5b) aka process.c
=======================================================================
gcc -Wall -o process process.c gir ingen error

cppcheck --enable=all ./process.c gir ingen error

clang-tidy-5.0 -checks='*' process.c -- -std=c11 gir kun error om stdeff.h not found

valgrind --leak-check=yes ./process gir ingen error

valgrind --tool=helgrind ./process gir ingen error


Lage nye trader og enkel semafor-synkronisering av disse (5.6a) aka thread.c
=======================================================================
gcc -Wall -pthread -o thread thread.c gir ingen error

clang-tidy-5.0 -checks='*' thread.c -- -std=c11 gir ingen error

valgrind --leak-check=yes ./thread gir ingen error

valgrind --tool=helgrind ./thread gir ingen error


Flere Producere og Consumere (5.6b) aka prod.c
=======================================================================
gcc -Wall -pthread -o prod prod.c gir ingen error

cppcheck gir bare error på manglende headerfiler som fortsatt er der

Output fra valgrind --leak-check=yes ./prod
possibly lost: 2,176 bytes in 8 blocks

Output fra valgrind --tool=helgrind ./prod
ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 25882 from 46)
